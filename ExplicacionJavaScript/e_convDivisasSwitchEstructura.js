

let $pesos1Dolar = 4611.88;
let historicoDolar = [$pesos1Dolar];

let pesosDolar = document.getElementById("pesos1Dolar")
pesosDolar.innerHTML +=`<h4>un peso colombiano equivale a $ ${$pesos1Dolar} pesos.</h4>`;

let cambioDivisa = document.getElementById("cambioDivisa")
let histDolar = document.getElementById("histDolar")

let menu = "1. Convertir de Pesos a Dolares.\n" +
            "2. Convertir de Dolares a Pesos. \n" +
            "3. Actualizar el Precio del Dolar.\n" +
            "4. Mostrar el Historico del Dolar.\n" +
            "0. Salir."

let pesos, dolares;
do{
    menuOpc = prompt (menu, "0")

    switch(menuOpc){
        case "1":
            //Bloque de codigo onvertir de Pesos a Dolares
            pesos = parseFloat(prompt("Digite la cantidad de Pesos:", "10000"));
            dolares = Math.round (pesos * 1 / $pesos1Dolar)
            cambioDivisa.innerHTML = `
                <h2>Convertir de pesos a Dolares </h2>
                <h4> Usted tiene $${dolares} US </h4>
            `;
            break;
        case "2":
            //Bloque de codigo Convertir de Dolares a Pesos.
            dolares = parseFloat(prompt("Digite la cantidad de Pesos:", "10000"));
            pesos = Math.round (dolares * $pesos1Dolar)
            cambioDivisa.innerHTML = `
                <h2>Convertir de pesos a Dolares </h2>
                <h4> Usted tiene $${pesos} US </h4>
            `;
            break;
        case "3":
            //Bloque de codigo Actualizar el Precio del Dolar
            $pesos1Dolar = parseFloat(prompt("Actualice el precio del dolar:", "5000"))
            pesosDolar.innerHTML +=`<h4>un peso colombiano equivale a $ ${$pesos1Dolar} pesos.</h4>`;
            historicoDolar.push($pesos1Dolar);
            break;
        case "4":
            //Bloque de codigo Mostrar el Historico del Dolar.
            histDolar.innerHTML = "<h2> Historico del dolar</h2>";
            for(let i = 0; i < historicoDolar.length; i++){
                histDolar.innerHTML +=`
                    <h4>Registro ${i+1}: $${historicoDolar[i]} </h4>
                `;    
            }
            break;
        case "0":
            //Bloque de codigo Salir
            break;
        default:
            //Bloque de codigo
            alert("Seleccione una opcion valida del menu")
            break
    }
}while(menuOpc !=0)