
//Importar mongoose
const mongoose = require('mongoose');

//importar las variables de  entorno
require('dotenv').config('../var.env');

//Establecer la conexion en una funcion
const conexionDB = async () => {

    try {
        //await espera la peticion de la funcion => conectar
        await mongoose.connect(process.env.URI_MONGODB);

        console.log("Base de Datos conectada")

    } catch (error) {

        console.log("Error: " + error);
        //para salir del proceso de conexion
        process.exit(1);
    }
}

// exportarlo para que se pueda utilizar desde otros archivos
module.exports = conexionDB;



