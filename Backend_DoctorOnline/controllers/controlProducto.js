
//importar modelo del producto
const modelProducto = require('../models/modelProducto');

//CRUD => Create
exports.crear = async (req, res) => {

    try {
        
        let producto = new modelProducto(req.body);
        // guardar en la BD
        await producto.save();

        // respuesta de verificacion
        res.send(producto);

    } catch (error) {
        console.log("Error Guardando Datos: " + error);
        res.status(500).send("Error al guardar el producto...");
    }
}


//CRUD => Read
exports.obtener = async (req, res) => {
    try {
        
        //consultar BD
        const producto = await modelProducto.find();
        //el retorno convertir a tipo Json
        res.json(producto);

    } catch (error) {
        console.log("Error Obtener/leer Datos: " + error);
        res.status(500).send("Error al obtener/leer el producto...");
    }
}
exports.obtenerPorId = async (req, res) => {

    try {
        
        //validar si existe el documento
        const producto = await modelProducto.findById(req.params.id);

        // verificar que existe el producto 
        if(!producto){
            res.status(404).json({mgs : "producto no existe"})
        }
        else{
            //consultar BD
            const producto = await modelProducto.findById(req.params.id);
            //el retorno convertir a tipo Json
            res.json(producto);
        }

    } catch (error) {
        console.log("Error Obtener/leer Datos: " + error);
        res.status(500).send("Error al obtener/leer el producto...");
    }
}


//CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        
        //validar si existe el documento
        const producto = await modelProducto.findById(req.params.id);

        // verificar que existe el producto 
        if(!producto){
            res.status(404).json({mgs : "producto no existe"})
        }
        else{
            await modelProducto.findByIdAndUpdate({_id: req.params.id}, req.body);
            //mensaje de confirmacion
            res.json({mensaje: "Producto actualizado"});
        }
    } catch (error) {
        console.log("Error al actualizar Datos: " + error);
        res.status(500).send("Error al actualizar el producto...");
    }
}



//CRUD => Delete
exports.eliminar = async (req, res) => {

    try {
        
        //validar si existe el documento
        const producto = await modelProducto.findById(req.params.id);

        // verificar que existe el producto 
        if(!producto){
            res.status(404).json({mgs : "producto no existe"})
        }
        else{
            //sentencia para eliminar el producto
            await modelProducto.findByIdAndRemove({_id: req.params.id});
            //mensaje de confirmacion
            res.json({mensaje: "Producto eliminado"});
        }
    } catch (error) {
        console.log("Error al eliminar Datos: " + error);
        res.status(500).send("Error al eliminar el producto...");
    }
}


