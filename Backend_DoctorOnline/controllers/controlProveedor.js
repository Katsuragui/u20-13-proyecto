
//Importar el modelo del proveedor
const modelProveedor = require('../models/modelProveedor');

// podemos exportar directamente los metodos
//CRUD => crear
exports.crear = async (req, res) =>{

    try{
        let proveedor;

        //establecemos los datos a guardar
        console.log("req.body = " + req.body);
        proveedor = new modelProveedor(reo.body);
        // proveedor= new modelProveedor({
        //     id_prov : 13,
        //     nombre_prov : "UIS",
        //     telefono_prov : 8798297,
        //     direccion_prov : "cra 87c #55-52",
        //     ciudad_prov : "Bogota"
        // });


        //guardamos en la bse de datos
        await proveedor.save();

        // respuesta para verificar la avriable
        res.send(proveedor);

    } catch (error) {

        console.log("Error al guardar Datos: " + error)
        res.status(500).send("Error al guardar el proveedor")
    }
}
exports.obtenerPorId = async (req, res) => {

    try {
        
        //validar si existe el documento
        const proveedor = await modelProveedor.findById(req.params.id);
        console.log("proveedor = " + proveedor);

        // verificar que existe el proveedor
        if(!proveedor){
            res.status(404).json({mgs : "Proveedor no existe"})
        }
        else{
            //consultar BD
            const proveedor = await modelProveedor.findById(req.params.id);
            //el retorno convertir a tipo Json
            res.json(producto);
        }

    } catch (error) {
        console.log("Error Obtener/leer Datos: " + error);
        res.status(500).send("Error al obtener/leer el producto...");
    }
}

// CRUD => Read
exports.obtener = async (req, res) => {

    try{

        //consulta a la base de datos
        const proveedor = await modelProveedor.find();
        // lo que retorna de la base de datos en la variable proveedor lo convierto Json
        res.json(proveedor);


    }catch (error){

        console.log("Error al obtener/leer los Datos: " + error)
        res.status(500).send("Erros al obtener/leer el proveedor")
    }
}


//CRUD => Update
exports.actualizar = async (req, res) =>{
    try {
        
        // Validar dato/registro/documento a actulaizar
        const proveedor = await modelProveedor.findById(req.params.id);
        //console.log('Proveedor= '+ proveedor )

        // validar si no existe el proveedor
        if(!proveedor){
            res.status(404).json({msg: "El proveedor no existe..."});
        }
        else {

            await modelProveedor.findByIdAndUpdate({_id: req.params.id}, req.body);
            //await modelProveedor.findByIdAndUpdate({_id: req.params.id_prov}, {nombre_prov:  "UIS MisionTIC 2022"});
            //mensaje de confirmacion de la actualizacion de los datos
            res.json({mensaje: "Proveedor actualizado correctamente..."})
        }

    } catch (error) {

        console.log("Error al actualizar los Datos: " + error);
        res.status(500).send("Erros al actyalizar el proveedor");
    }
}

//CRUD => Delete
exports.eliminar =async (req, res) => {
    
    try {
        
        // Validar dato/registro/documento a actulaizar
        const proveedor = await modelProveedor.findById(req.params.id);

        //Validar si proveedor no existe
        if(!proveedor){
            res.status(404).json({mensaje: 'El proveedor no existe...'});
        }
        else{
            await modelProveedor.findByIdAndDelete({_id: req.params.id})
            res.json({msg: "Proveedor eliminado..."})
        }

    } catch (error) {
        
        console.log("Error al Borar los Datos: " + error);
        res.status(500).send("Erros al Borrar el proveedor");
    }
}