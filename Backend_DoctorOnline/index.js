

//Importar el modulo de express
//forma convencional de nodejs
const express = require('express');
// otra forma de JavaScript
//import { Express } from 'express';

// Importar mongoose para la conexion a mongodb atlas
const mongoose = require ('mongoose'); 

//Importacion de las variables de entorno
require('dotenv').config({path: 'var.env'});

//para establecer las rutas de express
const  router = express.Router();

//importacion de la ruta de la configuracion con la base de datos
const conectarDB = require('./config/cxn_db'); 

//para inicializar express
var app = express ();

//enviar Json
app.use(express.json());

//Importar los CORS
const cors = require ('cors');
app.use(cors());

// //solucion cors => Tomado de (Configuracion de CORS asincronicamente:):
// //https://www.it-swarm-es.com/es/javascript/como-habilitar-cors-nodejs-con-express/830937099/
// var whitelist =['http://localhost:4000/apirest/', 'http://localhost:4000/apirest']
// var corsOptionsDelegate= function (rep, callback){
//     var corsOptions;
//     if(whitelist.indexOf(rep.header('Origin')) !== -1){
//         corsOptions = { Origin: true}
//     } 
//     else {
//         corsOptions ={ Origin: false}
//     }
//     callback(null, corsOptions)
// }



//Establecer la coneccion con la base de datos
conectarDB();

//Especificamos que l acarpeta publica (public) es nuestro contenido del frontend
app.use(express.static('public'));


//------------------------------------------------------------------------------------------------------------------------------------
//Prueba minimapara saber que funciona el modulo de express 
//en relacion al entorno web (ejecutar el servidor web)
// app.use('/',function(req, res){
//      res.send("Hola Mundo")
//  });
//utilizando una funcio flecha
// app.use('/', (req, res) => {
//     res.send("Utilizando una funcion flecha")
// });
//------------------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------------------
//Para utilizar los metodos http => GET,PORT, PUT, DELETE, etc...
//const router = express.Router(); => agregada en la linae 10antes de iniciatlizar express()
app.use(router);


//Importar el controlador de Proveedores=> CRUD
const controlProveedor = require('./controllers/controlProveedor');

// router.post('/apirest/proveedor/', cors(corsOptionsDelegate), controlProveedor.crear);
// router.get('/apirest/proveedor/', cors(corsOptionsDelegate), controlProveedor.obtener);
// router.get('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.obtenerPorId)
// router.put('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.actualizar);
// router.delete('/apirest/proveedor/:id', cors(corsOptionsDelegate), controlProveedor.eliminar);

router.post('/apirest/proveedor/', controlProveedor.crear);
router.get('/apirest/proveedor/', controlProveedor.obtener);
router.get('/apirest/proveedor/:id', controlProveedor.obtenerPorId)
router.put('/apirest/proveedor/:id', controlProveedor.actualizar);
router.delete('/apirest/proveedor/:id', controlProveedor.eliminar);

//Importar el controlador de productos => CRUD
const controlProducto =require('./controllers/controlProducto');

router.post('/apirest/producto/', controlProducto.crear); //Create
router.get('/apirest/producto/', controlProducto.obtener); //Read
router.get('/apirest/producto/:id', controlProducto.obtenerPorId); //Read
router.put('/apirest/producto/:id', controlProducto.actualizar); //Update
router.delete('/apirest/producto/:id', controlProducto.eliminar); //Delete

// router.post('/apirest/producto/', cors(corsOptionsDelegate), controlProducto.crear); //Create
// router.get('/apirest/producto/', cors(corsOptionsDelegate), controlProducto.obtener); //Read
// router.get('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.obtenerPorId); //Read
// router.put('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.actualizar); //Update
// router.delete('/apirest/producto/:id', cors(corsOptionsDelegate), controlProducto.eliminar); //Delete

// router.get('/mensaje', function(req, res){
//     res.send('Mensaje con metodo GET')

//     // const nome_db = 'Prueba' 
//     // const user = 'admin123'
//     // const psw = 'admin123'
//     // const uri = `mongodb+srv://admin123:admin123@misionticu20-13.gcia3sm.mongodb.net/Prueba?retryWrites=true&w=majority`
//     // mongoose.connect(uri)
//     // mongoose.connect(process.env.URI_MONGODB)
//     //     .then(function(){console.log("Base de datos conectada")})
//     //     .catch(function(e){console.log("Error:" + e)})

//     // //Establecer la coneccion con la base de datos
//     // conectarDB();
// });

// router.post('/mensaje', function(req, res){
//     res.send('Mensaje con metodo POST')
// });

// router.put('/mensaje', function(req, res){
//     res.send('Mensaje con metodo PUT')
// });

// router.delete('/mensaje', function(req, res){
//     res.send('Mensaje con metodo DELETE')
// });

//------------------------------------------------------------------------------------------------------------------------------------

//Asignacion de puerto para el servidor web.
//app.listen(4000);
//Utikizando la variable de entorno del puerto
app.listen(process.env.PORT);

//Mensaje para saber que el servidor web esta activo
console.log('Servidor web Ejecutandose desde: http://localhost:4000/');
console.log('"Ctrl + c" para finalizar el servidor web');

