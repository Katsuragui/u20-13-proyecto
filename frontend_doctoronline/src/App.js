// import logo from './logo.svg';
import './App.css';
import ProductosPage from './pakages/productos';
import ProveedoresPage from './pakages/proveedores';
import IndexPage from './pakages/indexpage';
// import { Route } from 'react-router-dom';
// Para seleccionar unicamente la ruta seleccionada
import {Routes, Route} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header>
        <img width="800px" src="https://lms.uis.edu.co/mintic2022/pluginfile.php/1/theme_edumy/headerlogo1/1663168415/MisionTIC-UIS.png" alt="logo" />
        <hr />
      </header>

        <div>
          <Routes>
            <Route path='/' element={<IndexPage />}/>
            <Route path='/listaProductos' element={<ProductosPage />}/>
            <Route path='/listaProveedores' element={<ProveedoresPage />}/>
          </Routes>
          {/* <IndexPage /> */}
          {/* <ProductosPage /> */}
          {/* <ProveedoresPage /> */}
        </div>

        {/* <h1>
          Hola Tripulantes
        </h1> */}
     
    </div>
  );
}

export default App;
