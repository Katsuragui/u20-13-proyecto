

import { Link } from "react-router-dom";

const Menu = () =>{
    return (
        <ul className="nav justify-content-end">
            <li className="nav-item">
                {/* <a className="nav-link active" aria-current="page" href="/">Home</a> */}
                <Link className="nav-link active" aria-current="page" to="/">Leer Citas</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/listaProductos">Crear Citas</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/listaProveedores">Modificar Citas</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link disabled">Cacelar Citas</Link>
            </li>
        </ul>

    );
}


export default Menu;