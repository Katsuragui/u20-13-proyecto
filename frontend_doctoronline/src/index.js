import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
// Establecer las rutas o enlaces de la aplicacion
//previamente instalas => npm i react-router-dom
import {BrowserRouter as Router} from 'react-router-dom';

// import ProductosPage from './pakages/productos';
// import ProveedoresPage from './pakages/proveedores';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
  <Router>
     <App />
  </Router>
   
  // </React.StrictMode>
);

// const productos =ReactDOM.createRoot(document.getElementById('productos'));
// productos.render(
//   <ProductosPage />
// );

// const proveedores =ReactDOM.createRoot(document.getElementById('proveedores'));
// proveedores.render(
//   <ProveedoresPage var2= "contenido de ejemplo 2..."/>
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
